# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

#Make an array from the smallest to largest, and then remove the number if it
#is in nums.

def missing_numbers(nums)
  (nums[0]..nums[-1]).to_a.reduce([]) do |acc, el|
    acc << el if !nums.include?(el)
    acc
  end
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

#1100, split, reverse, 0 0 1 1, then map (2**[idx] * el) and then sum
#

def base2to10(binary)
    arr = []
    binary.split('').reverse.each_with_index do |e, i|
      arr << e.to_i * 2**(i)
    end
    arr.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    s = Hash.new()
    self.each do |k,v|
      s[k] = v if yield(k,v)
    end
    s
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  #self, hash, merge
  #Default merging behavior:
  #specified merging: self[k] = yield(k,self[k],hash[k])

  #use self, for each element in hash, put it into self.

  def my_merge(hash, &prc)
    h = self
    h.default = 0
    hash.each do |k,v|
      block_given? ? h[k] = prc.call(k,h[k],v) : h[k] = v 
    end
    h
  end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

#index -7 go from 0 to -7 and find the next number each time.
#In the other direction: go from 0 to seventh

def lucas_numbers(n)
  arr = [2,1]
  if n == 0; arr[0]
  elsif n == 1; arr[1]
  elsif n>0
    (n-1).times {arr << (arr[-1] + arr[-2])}; arr[-1]
  elsif n<0
      (n*-1).times {arr.unshift(arr[1]-arr[0])}; arr[0]
  end

end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

#Find the longest palindrome.
#Find all the palindromes and then return the largest.
#Find a palindrome, then find then next one and swap if the next one is longer.
# "noonminimum" find minim

def longest_palindrome(string)
  palindrome = ""
  start_index = 0

  until start_index == string.length - 2
    found_p = string[start_index]
    string[(start_index+1)..-1].each_char do |ch|
        found_p += ch
        if found_p == found_p.reverse
            palindrome = found_p if found_p.length > palindrome.length
          end
    end
    start_index += 1
  end

  return false if palindrome.length < 2

  palindrome.length

end
